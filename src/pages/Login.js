import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';


export default function Login() {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	function authenticate(e) {
		
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: `Welcome ${email}!`,
					icon: "success"
				})
			} else {
				Swal.fire({
					title: "Please check you login details.",
					icon: "error"
				})
			}
		})

		setEmail("");
		setPassword("");		 
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
			headers: {
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id, 
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if((email !== "" && password !== "" )) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	if(user.id !== null && user.isAdmin === false) {
		return <Navigate to="/products" />

	} else if (user.id !== null && user.isAdmin === true){
		return <Navigate to="/dashboard" />

	} else {
		return (
			<Form onSubmit={e => authenticate(e)}>

				<h1 className="text-center my-3">Login</h1>
				  
			    <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email"
			        	value={email}
			        	onChange={e => setEmail(e.target.value)}
			        	required
			        />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password"
			        	value={password}
			        	onChange={e => setPassword(e.target.value)}
			        	required
			        />
			    </Form.Group>

			    {
			      	isActive ?
			      		<Button variant="primary" type="submit" id="submitBtn">
			      		  Submit
			      		</Button>
			      		:
			      		<Button variant="primary" type="submit" id="submitBtn" disabled>
			      		  Submit
			      		</Button>
			    }
			</Form>			
		)
	}
}