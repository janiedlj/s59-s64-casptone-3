import { useEffect, useState } from 'react';
import { Row, Col, CardGroup } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {

			const sortData = data.sort((a, b) => (a.productName > b.productName) ? 1 : -1);

			setProducts(sortData.map(product => {
				return (
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	}, [])
		
	return (

		<>
			<h1>Products</h1>
		     {products}
		</>

	)
}