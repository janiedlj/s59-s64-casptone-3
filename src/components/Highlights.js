import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights(){
	return (


	    <Row className="mt-3 mb-3">
	    	<Col xs={12} md={4}>
	    		<Card className="cardHighlight p-3">
	    		<Card.Img variant="top" src="https://i.pinimg.com/474x/43/18/42/431842f43383db788c842a24fb4b7c26.jpg" />
	    		  <Card.Body>
	    		    <Card.Title>Dresses</Card.Title>
	    		    <Card.Text>
	    		      Dresses for those who like to standout in the crowd.
	    		    </Card.Text>
	    		  </Card.Body>
	    		</Card>
	    	</Col>

	    	<Col xs={12} md={4}>
	    		<Card className="cardHighlight p-3">
	    		<Card.Img variant="top" src="https://lzd-img-global.slatic.net/g/p/3115429402e0fbae0cdccf0dc004350d.jpg" />
	    		  <Card.Body>
	    		    <Card.Title>Pants</Card.Title>
	    		    <Card.Text>
	    		      Pants for women who want to show dominance with femininity.
	    		    </Card.Text>
	    		  </Card.Body>
	    		</Card>
	    	</Col>

	    	<Col xs={12} md={4}>
	    		<Card className="cardHighlight p-3">
	    		<Card.Img variant="top" src="https://ph-test-11.slatic.net/p/8a04b760929a0d47250ac08e63cb74ad.jpg" />
	    		  <Card.Body>
	    		    <Card.Title>Tops</Card.Title>
	    		    <Card.Text>
	    		      Tops for those who want to mix and match.
	    		    </Card.Text>
	    		  </Card.Body>
	    		</Card>
	    	</Col>
	    </Row>


	);
}